var express = require("express");
var bodyParser = require('body-parser')
var app= express();
var path = require('path');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

app.use(express.static(path.join(__dirname, 'public')));
app.set('/public/views', __dirname + '/views');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var db = 'mongodb://localhost/hrAdminData';
mongoose.connect(db);

var userSchema = new Schema({
	emp_id:{type:String,unique:true,required: true},
	first_name: {type: String, required: true},
	last_name : String,
	email :{type: String, required: true, unique: true},
	phone_number:String
});
var projectSchema = new Schema({
	project_name:{type:String,unique:true,required: true},
	start_date: {type: Date, required: true},
	end_date : {type: Date, required: true},
	client_name :{type: String, required: true},
  modelsd:[]
});


var User = mongoose.model('User',userSchema,'employees');
var Project = mongoose.model('Project',projectSchema,'projects');


app.get('/', function(req,res){
	res.sendFile(__dirname + '/' + 'index.html');
});

var member = {
  email: 'node@gmail.com',
  password: 'password'
}


app.post("/hr-signin" ,function(req,res){
	if(req.body.email == member.email && req.body.password == member.password){
		res.redirect('/dashboard');
	}else{
		res.send('incorrect user/password');
	}
});

app.post('/add-employee',function(req, res, next){
	var user = new User({
	emp_id: req.body.emp_id,
    first_name: req.body.first_name,
    last_name : req.body.last_name,
	email : req.body.email,
	phone_number: req.body.phone_number
  });

  user.save(function(err) {
    if (err) return next(err);
  });
   /*res.redirect('/dashboard');*/
});

app.post('/update-employee', function(req, res) {
    var empToUpdate=req.body.emp_id;

    User.update({emp_id:empToUpdate},req.body, function(err, result){
        /*res.redirect('/dashboard');*/
    });
});

app.get('/api/users-list', function (req, res) {
    console.log('I received a GET request');

    User.find(function(err, employees) {
      console.log(employees);
        if(!err){
           res.json(employees);
        }
    });

});

app.post('/add-project',function(req, res, next){
	var project = new Project({
	project_name: req.body.project_name,
    start_date: req.body.start_date,
    end_date : req.body.end_date,
	client_name : req.body.client_name,
  modelsd:req.body.list.items
  });

  project.save(function(err) {
    if (err) return next(err);
  });
   res.redirect('/dashboard#projects');
});

app.post('/update-project', function(req, res) {
    var projectToUpdate=req.body.project_name;

    Project.update({project_name:projectToUpdate},req.body, function(err, result){
        res.redirect('/dashboard');
    });
});

app.get('/api/projects-list', function (req, res) {
    console.log('I received a GET request');

    Project.find(function(err, projects) {
      console.log(projects)
        if(!err){
           res.json(projects);
        }
    });

});


app.get('/logout', function (req, res) {
  res.redirect('/');
});


app.get('*', function(req, res) {
  res.redirect('/#' + req.originalUrl);
});

app.listen(3001,function(){
	console.log("server listening: 3001");
});

module.exports = User;
module.exports = Project;

