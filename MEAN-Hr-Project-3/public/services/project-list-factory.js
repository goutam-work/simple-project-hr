angular.module('MyApp')
  .factory('projectsFactory', function ($q, $http) {
    return {
      getProjectStuff: function () {
        var deferred = $q.defer(),
          httpPromise = $http.get('api/projects-list');
 
        httpPromise.success(function (projects) {
          deferred.resolve(projects);
        })
          .error(function (error) {
            console.error('Error: ' + error);
          });
        return deferred.promise;
      }
    };
  });