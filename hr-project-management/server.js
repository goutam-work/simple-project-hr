var express = require("express");
var bodyParser = require('body-parser')
var app= express();
var mongoose = require('mongoose');
/*var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;*/
var Schema = mongoose.Schema;

app.use('/public', express.static('public'));
app.set('views', __dirname + '/views');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/*var url = 'mongodb://localhost/hrData';*/

mongoose.connect('mongodb://localhost/hrData');

/*MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  insertDocument(db, function() {
      db.close();
  });
});*/

var userSchema = new Schema({
	first_name: {type: String, required: true, unique: true},
	last_name : String,
	email :{type: String, required: true, unique: true},
	phone_number:String,
	user_role:String,
	project_name:{type: String, required: true, unique: true}
});
var User = mongoose.model('User',userSchema,'users');

/*var insertDocument = function(db, callback) {
   db.collection('users').insert(function(err, result) {
    assert.equal(err, null);
    console.log("Inserted a user into the users collection.");
    callback();
  });
};*/


app.get('/', function(req,res){
	res.sendFile(__dirname + '/' + 'index.html');
});



app.post("/hr-signin" ,function(req,res){
	var email = req.body.email;
	var password = req.body.password;
	if(email === 'node@gmail.com' && password === 'password'){
		res.redirect('/dashboard');
	}else{
		res.send('incorrect user/password');
	}
});

app.get('/dashboard',function(req,res){
	res.sendFile(__dirname+ '/'+ 'views/dashboard.html');
});
app.get('/users-list',function(req,res){
	res.sendFile(__dirname+ '/'+ 'views/users-list.html');
});

app.post('/add-employee',function(req, res, next){
	var user = new User({
    first_name: req.body.first_name,
    last_name : req.body.last_name,
	email : req.body.email,
	phone_number: req.body.phone_number,
	user_role: req.body.user_role,
	project_name: req.body.project_name
  });

  user.save(function(err) {
    if (err) return next(err);
    /*res.sendStatus(200);*/
  });
  app.get('/users-list', user.findAll);
  res.redirect('/dashboard');
});



exports.findAll = function(req, res) {
    db.collection('users', function(err, collection) {
        collection.find().toArray(function(err, persons) {
            res.send(persons);
        });
    });
};


app.get('/logout', function (req, res) {
  res.redirect('/');
});

app.listen(3001,function(){
	console.log("server listening: 3001");
});

module.exports = User;