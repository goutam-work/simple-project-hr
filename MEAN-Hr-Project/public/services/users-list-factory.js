angular.module('MyApp')
  .factory('usersFactory', function ($q, $http) {
    return {
      getUserStuff: function () {
        var deferred = $q.defer(),
          httpPromise = $http.get('api/users-list');
 
        httpPromise.success(function (users) {
          deferred.resolve(users);
        })
          .error(function (error) {
            console.error('Error: ' + error);
          });
        return deferred.promise;
      }
    };
  });