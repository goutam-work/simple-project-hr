angular.module('MyApp')
  .controller('UserDetailCtrl', function($scope,usersFactory) {

    $scope.usersList = {};
    usersFactory.getUserStuff().then(function (users) {
    $scope.usersList = users;
    }, function (error) {
      console.error(error);
    });
  });