var express = require("express");
var bodyParser = require('body-parser')
var app= express();
var path = require('path');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

app.use(express.static(path.join(__dirname, 'public')));
app.set('/public/views', __dirname + '/views');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/hrData');

var userSchema = new Schema({
	first_name: {type: String, required: true, unique: true},
	last_name : String,
	email :{type: String, required: true, unique: true},
	phone_number:String,
	user_role:String,
	project_name:{type: String, required: true, unique: true}
});

var User = mongoose.model('User',userSchema,'users');

app.get('/', function(req,res){
	res.sendFile(__dirname + '/' + 'index.html');
});

var member = {
  email: 'node@gmail.com',
  password: 'password'
}


app.post("/hr-signin" ,function(req,res){

	if(req.body.email == member.email && req.body.password == member.password){
		res.redirect('/dashboard');
	}else{
		res.send('incorrect user/password');
	}
});


app.post('/add-employee',function(req, res, next){
	var user = new User({
    first_name: req.body.first_name,
    last_name : req.body.last_name,
	email : req.body.email,
	phone_number: req.body.phone_number,
	user_role: req.body.user_role,
	project_name: req.body.project_name
  });

  user.save(function(err) {
    if (err) return next(err);
  });
   res.redirect('/dashboard');
});


app.get('/api/users-list', function (req, res) {
    console.log('I received a GET request');

    User.find(function(err, users) {
      console.log(users)
        if(!err){
           res.json(users);
        }
    });1
});


app.get('/logout', function (req, res) {
  res.redirect('/');
});


app.get('*', function(req, res) {
  res.redirect('/#' + req.originalUrl);
});

app.listen(3001,function(){
	console.log("server listening: 3001");
});

module.exports = User;