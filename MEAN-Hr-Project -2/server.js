var express = require("express");
var bodyParser = require('body-parser');
var app= express();
var path = require('path');
var jwt = require('jwt-simple');
var moment = require('moment');
var logger = require('morgan');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var crypto = require('crypto');
var bcrypt = require('bcryptjs');

var async = require('async');
var request = require('request');
var xml2js = require('xml2js');



app.use(express.static(path.join(__dirname, 'public')));
app.set('/public/views', __dirname + '/views');
/*app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
*/
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());


var member = {
  email: 'node@gmail.com',
  password: 'password'
}

JSON.stringify(member);
console.log(member);

function ensureAuthenticated(req, res, next) {
  if (req.headers.authorization) {
    var token = req.headers.authorization.split(' ')[1];
    try {
      var decoded = jwt.decode(token, tokenSecret);
      if (decoded.exp <= Date.now()) {
        res.send(400, 'Access token has expired');
      } else {
        req.member = decoded.member;
        return next();
      }
    } catch (err) {
      return res.send(500, 'Error parsing token');
    }
  } else {
    return res.send(401);
  }
}

function createJwtToken(member) {
  var payload = {
    member: member,
    iat: new Date().getTime(),
    exp: moment().add('days', 7).valueOf()
  };
  return jwt.encode(payload, tokenSecret);
}



mongoose.connect('mongodb://localhost/hrData');

var userSchema = new Schema({
	first_name: {type: String, required: true, unique: true},
	last_name : String,
	email :{type: String, required: true, unique: true},
	phone_number:String,
	user_role:String,
	project_name:{type: String, required: true, unique: true}
});

var User = mongoose.model('User',userSchema,'users');

app.get('/', function(req,res){
	res.sendFile(__dirname + '/' + 'index.html');
});



app.post("/hr-signin" ,function(req,res){
	console.log(member)
	if(req.body.email == member.email && req.body.password == member.password){
		res.redirect('/dashboard');
		var token = createJwtToken(member);
  	res.send({ token: token });
	}else{
		res.send('incorrect user/password');
	}
	 
});


app.post('/add-employee',function(req, res, next){
	var user = new User({
    first_name: req.body.first_name,
    last_name : req.body.last_name,
	email : req.body.email,
	phone_number: req.body.phone_number,
	user_role: req.body.user_role,
	project_name: req.body.project_name
  });

  user.save(function(err) {
    if (err) return next(err);
  });
   res.redirect('/dashboard');
});


app.get('/api/users-list', function (req, res) {
    console.log('I received a GET request');

    User.find(function(err, users) {
      console.log(users)
        if(!err){
           res.json(users);
        }
    });1
});


app.get('/logout', function (req, res) {
  res.redirect('/');
});


app.get('*', function(req, res) {
  res.redirect('/#' + req.originalUrl);
});

app.listen(3002,function(){
	console.log("server listening: 3002");
});

module.exports = User;